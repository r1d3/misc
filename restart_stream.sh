#!/bin/sh

killall mjpg_streamer
cd /usr/local/share/agayon/mjpg_streamer

if test -f "/srv/http/ngnix/r1d3/public_html/static/lidar.png"; then
    mv "/srv/http/ngnix/r1d3/public_html/static/lidar.png" /srv/http/ngnix/r1d3/public_html/static/archives/lidar_$(date -d "today" +"%Y-%m-d_%H-%M").png
fi

export LD_LIBRARY_PATH="$(pwd)"

./mjpg_streamer -i "./input_uvc.so -f 10" -o "./output_http.so -p 8090 -w ../www_agayon"  > /tmp/stream.1 2>/tmp/stream.out &



exit 0
